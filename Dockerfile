FROM ruby:2.7.2-alpine3.13

ARG RAILS_ENV=development
ARG RAILS_SERVE_STATIC_FILES=0
ENV RAILS_ENV $RAILS_ENV
ENV RAILS_SERVE_STATIC_FILES $RAILS_SERVE_STATIC_FILES
ENV RAILS_LOG_TO_STDOUT 1
ENV TZ Asia/Tokyo
ENV LANG ja_JP.UTF-8
ENV LC_ALL C.UTF-8
ENV APP_DIR /app
WORKDIR $APP_DIR

COPY Gemfile* $APP_DIR/

RUN set -ex && \
    apk add --no-cache --virtual build-dependencies build-base && \
    apk add --no-cache \
        git \
        mariadb-dev \
        nodejs \
        tzdata && \
    gem install bundler:1.17.3 && \
    bundle install --jobs=4 --retry 4 && \
    apk del --purge build-dependencies

COPY . $APP_DIR
RUN rails assets:precompile
VOLUME /app/tmp
VOLUME /app/public

CMD ["rails", "server"]
