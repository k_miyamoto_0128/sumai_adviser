Rails.application.configure do
  config.lograge.enabled = true

  # If you're using Rails 5's API-only mode and inherit from ActionController::API, you must define it as the controller base class which lograge will patch:
  # config.lograge.base_controller_class = 'ActionController::API'

  config.lograge.formatter = Lograge::Formatters::Logstash.new

  config.lograge.custom_payload do |controller|
    params = controller.request.params.except(* %w[controller action])

    {
      request_id: controller.request.request_id,
      params: params
    }
  end

  config.lograge.custom_options = lambda do |event|
    ret = {
      request_ip: event.payload[:request_ip],
      error_message: nil,
      error_stacktrace: nil
    }

    if (error = event.payload[:exception_object])
      ret.merge!(
        error_message: error.message,
        error_stacktrace: error.backtrace.join("\n")
      )
    end

    ret
  end
end
