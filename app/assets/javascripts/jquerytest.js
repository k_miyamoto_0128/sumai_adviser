$(function () {
  //始めにjQueryで送信ボタンを無効化する
  $(".submit").prop("disabled", true);

  //始めにjQueryで必須欄を加工する
  $("#modal-a input:required").each(function () {
    $(this).prev("label").addClass("required");
  });

  //入力欄の操作時
  $("#modal-a input:required").change(function () {
    let isOK = false;
    // 空かどうかの判定
    let isBlank = [];
    $("#modal-a input:required").each(function (i, e) {
      isBlank.push($(this).val() === "");
    });
    let isFilled = isBlank.every((e) => !e);
    //メールアドレスの形になっているかの判定
    let email = $("#modal-a #email-a").val();
    const reg = /^[A-Za-z0-9]{1}[A-Za-z0-9_.-]*@{1}[A-Za-z0-9_.-]{1,}\.[A-Za-z0-9]{1,}$/;
    //最終判定 & 送信ボタンの有効化・無効化
    isOK = isFilled && reg.test(email);
    if (isOK) {
      //送信ボタンを復活
      $("#modal-a .submit").prop("disabled", false);
    } else {
      //送信ボタンを閉じる
      $("#modal-a .submit").prop("disabled", true);
    }
  });

  //始めにjQueryで必須欄を加工する
  $("#modal-b input:required").each(function () {
    $(this).prev("label").addClass("required");
  });

  //入力欄の操作時
  $("#modal-b input:required").change(function () {
    let isOK = false;
    // 空かどうかの判定
    let isBlank = [];
    $("#modal-b input:required").each(function (i, e) {
      isBlank.push($(this).val() === "");
    });
    let isFilled = isBlank.every((e) => !e);
    //メールアドレスの形になっているかの判定
    let email = $("#modal-b #email-b").val();
    const regEmail = /^[A-Za-z0-9]{1}[A-Za-z0-9_.-]*@{1}[A-Za-z0-9_.-]{1,}\.[A-Za-z0-9]{1,}$/;
    //電話番号の判定
    let tel = $("#modal-b #tel-b").val();
    const regTel = /^[0-9]{1,}$/;
    //最終判定 & 送信ボタンの有効化・無効化
    isOK = isFilled && regEmail.test(email) && regTel.test(tel);
    if (isOK) {
      //送信ボタンを復活
      $("#modal-b .submit").prop("disabled", false);
    } else {
      //送信ボタンを閉じる
      $("#modal-b .submit").prop("disabled", true);
    }
  });
});
